let step = 30;

let offset = 30; //画面の外側にはみ出して描画する分量

// setup関数 : 初回1度だけ実行される
function setup() {
  createCanvas(960, 540); // ウィンドウサイズを960px,540pxに
  colorMode(HSB, 360, 100, 100); // HSBでの色指定にする
}
// draw関数 : setup関数実行後繰り返し実行される
function draw() {
  randomSeed(1);
  fill(0, 0, 100, 40);
  for (let y = -offset; y <= height + offset; y = y + map(y, -offset, height + offset, step * 2, step / 2)) {
    stroke(0, 0, 100);
    strokeWeight(3);
    fill(random(160, 230), random(80, 100), random(80, 100), random(50, 100));
    
    //図形描画を開始
    beginShape();
    
    for (let x = -offset; x <= width + offset; x = x + step) {
      let n = noise(y * 0.1, x * 0.001, (y + frameCount) * 0.005);
      n = map(n, 0, 1, -height / 3, height / 3);
      let y2 = y + n;
      vertex(x, y2);
    }
    vertex(width + offset, height + offset);
    vertex(-offset, height + offset);
    endShape(CLOSE);
    //図形描画を終了
  }
}

function keyPressed() {
  // this will download the first 5 seconds of the animation!
  if (key === 'g') {
    saveGif('mySketch', 5);
  }
  else if(key == 'c'){
    saveCanvas('myCanvas', 'jpg');
  }
}