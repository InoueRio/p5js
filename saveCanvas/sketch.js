function setup() {
  let c = createCanvas(400, 400);
  print("setup!");  

  saveCanvas(c, 'myCanvas', 'jpg');

}

function draw() {
  background(220);
  circle(200,200,200);
  noStroke();

  print("frameCount:" + frameCount); 
}

function keyPressed() {
  if (key === 'S') {
    saveCanvas(c, 'myCanvas', 'jpg');
  }
}