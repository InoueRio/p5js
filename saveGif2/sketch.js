function setup() {
  createCanvas(400, 400);
}

function draw() {
  colorMode(RGB);
  background(30);

  // create a bunch of circles that move in... circles!
  for (let i = 0; i < 15; i++) {
    let opacity = map(i, 0, 10, 0, 255);
    noStroke();
    fill(230, 250, 90, opacity);
    circle(
      100 * sin(frameCount / (30 - i)) + width / 2,
      100 * cos(frameCount / (30 - i)) + height / 2,
      10
    );
  }
}

// you can put it in the mousePressed function,
// or keyPressed for example
function keyPressed() {
  // this will download the first 5 seconds of the animation!
  if (keyCode === 's') {
    saveGif('mySketch', 5);
  }
}