let step_size = 5;
let d = 4;

let pos_x;
let pos_y;

var NORTH = 0;
var NORTHEAST = 1;
var EAST = 2;
var SOUTHEAST = 3;
var SOUTH = 4;
var SOUTHWEST = 5;
var WEST = 6;
var NORTHWEST = 7;


function setup() {
//  createCanvas(windowWidth, windowHeight);
  createCanvas(windowWidth, windowHeight);
  colorMode(HSB, 360, 100, 100, 100);
  noStroke();

  pos_x = width/2;
  pos_y = height/2;
}

function draw() {
  for (var i = 0; i <= mouseX; i++) {
    let direction = int(random(3, 5));
    if (direction == EAST) {
      pos_x += step_size;
    } else if (direction == SOUTHEAST){
      pos_x += step_size;
      pos_y += step_size;
    } else if (direction == SOUTH){
      pos_y += step_size;
    }

    // check x, y in canvas
    if (pos_x > width){
      pos_x -= width
    }
    if (pos_y > height){
      pos_y -= height
    }

    // draw
    fill(0, 40);
    ellipse(pos_x + step_size/2, pos_y+ step_size/2, d, d);
  }
}

function keyPressed() {
  if (key === 's') {
    saveCanvas('myCanvas', 'jpg');
  }
  if (key === 'S') {
    saveGif('mySketch', 3);
  }
}