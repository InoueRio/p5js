
tile_count_x = 4
tile_count_y = 4


function setup() {
//  createCanvas(windowWidth, windowHeight);
  createCanvas(windowWidth, windowHeight);
  colorMode(HSB, 360, 100, 100, 100);
  rectMode(CENTER);
}

function draw() {
  clear();

  let count = mouseY/10
  let para = mouseY/height;

  let tile_width = width / tile_count_x;
  let tile_height = height / tile_count_y;

  draw_mode = 0;
  for(let grid_x=0; grid_x < tile_count_x; grid_x++){
    for(let grid_y=0; grid_y < tile_count_y; grid_y++){
      pos_x = grid_x*tile_width + tile_width/2;
      pos_y = grid_y*tile_height + tile_height/2;

      push();
      translate(pos_x, pos_y);
      
      switch(draw_mode){
        case 0:
          for(let i=0; i<count; i++){
            stroke(i*10, 80, 80, 80);
            rect(0, 0, tile_width, tile_height);
            scale(1 - 4/count);
            rotate(para * 0.5);
          }  
        case 1:
          console.log("TBD");
      }

      pop();    
    } 
  }
}

function keyPressed() {
  if (key === 's') {
    saveCanvas('myCanvas', 'jpg');
  }
  if (key === 'S') {
    saveGif('mySketch', 3);
  }
  if (keyCode == DOWN_ARROW) tile_count_y = max(tile_count_y - 1, 1);
  if (keyCode == UP_ARROW) tile_count_y += 1;
  if (keyCode == LEFT_ARROW) tile_count_x = max(tile_count_x - 1, 1);
  if (keyCode == RIGHT_ARROW) tile_count_x += 1;
}