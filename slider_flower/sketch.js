let sliders_array = [];

function setup() {
//  createCanvas(windowWidth, windowHeight);
  createCanvas(windowWidth, windowHeight);
  colorMode(RGB);
  sliders_array.push(new slider_flower(width/2, height/2));
}

function draw() {
  background(100, 100, 100);
  sliders_array.forEach(function(slider){
    slider.update();
  });
}

function mouseReleased(){
  // create flower at mouseX, mouserY 
  sliders_array.push(new slider_flower(mouseX, mouseY));
}

class slider_flower{
  constructor(x, y){
    this.x0 = x; 
    this.y0 = y;
    this.sliders = [];
    this.sin_ang = 0;

    this.r = random(40, 80);
    let skip = 30; //step between sliders
    let counter = 0;
    for (let i = 0; i < 360; i += skip){
      // console.log(i);
      this.ang = radians(i);
      this.x1 = cos(this.ang)*this.r;
      this.y1 = sin(this.ang)*this.r;

      // create the slider
      this.sliders[counter] = createSlider(0, 255, 50);
      console.log((this.x0 + this.x1) + "," + (this.y0 + this.y1));
      this.sliders[counter].position(this.x0 + this.x1, this.y0 + this.y1);
      this.sliders[counter].style('width', this.r + 'px');
      this.sliders[counter].style('transform', 'rotate(' + i + 'deg)');
      counter++;
    }
  }
  // function update
  update(){
    let offset = 0;
    // console.log(this.sliders);
    for (let i = 0; i < this.sliders.length; i++){
      // map the value along the sine wave to the slider values
      let x = map(sin(this.sin_ang + offset), -1, 1, 0, 255);
      this.sliders[i].value(x);
      offset += 0.050;
    }
    this.sin_ang += 0.1;
  }
}

function keyPressed() {
  if (key === 's') {
    saveCanvas('myCanvas', 'jpg');
  }
  if (key === 'S') {
    saveGif('mySketch', 3);
  }
}