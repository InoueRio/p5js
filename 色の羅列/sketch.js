let color_count = 20;
let hue_values = [];
let saturation_values = [];
let brightness_values = [];
let random_seed = 0;

function setup() {
  createCanvas(windowWidth, windowHeight);
  colorMode(HSB, 360, 100, 100, 100);
  noStroke();
}

function draw() {
  noLoop();
  randomSeed(random_seed);

  // ---colors
  for (let i = 0; i < color_count; i++) {
    if (i % 2 == 0) {
      hue_values[i] = random(0, 50);
      saturation_values[i] = 50;
      brightness_values[i] = random(15, 100);
    } else {
      hue_values[i] = 15;
      saturation_values[i] = random(20, 100);
      brightness_values[i] = 100;
    }
  }

  // ---add tile
  let counter = 0;
  let row_count = int(random(10, 35));
  let row_height = height / row_count; // height = windowHeight
  for (let i = 0; i <= row_count; i++) {
    console.log("row_count:" + i);
    // how many fragments
    let part_count = i + 1;
    let parts = [];

    for (let ii = 0; ii < part_count; ii++) {
    //   // sub fragments or not?
    //   if (random() < 0.05) {
    //     // take care of big values
    //     let fragments = int(random(2, 20));
    //     part_count = part_count + fragments;
    //     for (let iii = 0; iii < fragments; iii++) {
    //       parts.push(random(2));
    //     }
    //   } else {
    //     parts.push(random(2, 20));
    //   }
      parts.push(random(2, 15));
    }

    // calculate parts total 
    sum_parts_total = parts.reduce(function(a, x) {return a + x;});

    // draw rects
    let sum_parts_tmp = 0;
    for (let ii = 0; ii < parts.length; ii++) {
      sum_parts_tmp += parts[ii];

      let x = map(sum_parts_tmp, 0, sum_parts_total, 0, width);
      let y = row_height * i;
      let w = -map(parts[ii], 0, sum_parts_total, 0, width);
      let h = row_height;
      let ind = counter % color_count;
      console.log("ind:" + ind + ",x:" + x + ",w:" + w);
      col = color(hue_values[ind], saturation_values[ind], brightness_values[ind]);
      fill(col);
      rect(x, y, w, h);

      counter++;
    }
  }
}

function mouseReleased() {
  random_seed = random(1000);
  loop();
}
