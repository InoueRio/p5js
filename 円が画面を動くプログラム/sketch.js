
let num = 200;

let x = []; //ボールのX座標
let y = []; //ボールのY座標
let d = []; //ボールの大きさ
let sx = []; // X方向のspeed
let sy = []; // Y方向のspeed
let c = [];

function setup() {
  createCanvas(500, 500);
  colorMode(HSB, 360, 100, 100, 100);

  for (let i = 0; i < num; i++){
    x[i] = width / 2;
    y[i] = height / 2;
    sx[i] = random(-15, 15) / 5;
    sy[i] = random(-15, 15) / 5;
    c[i] = color(random(360), random(50, 80), 100);
    d[i] = random(10, 50);
  }
}

function draw() {
  background(0, 0, 0, 5);

  for (let i = 0; i < num; i++){
    x[i] = x[i] + sx[i];
    y[i] = y[i] + sy[i];
    fill(c[i]);
    noStroke();
    ellipse(x[i], y[i], d[i], d[i]);

    if (x[i] < 0 || x[i] > width){
      sx[i] = sx[i] * -1;
    }
  
    if (y[i] < 0 || y[i] > height){
      sy[i] = sy[i] * -1;
    }
  }

}

function keyPressed() {
  // this will download the first 5 seconds of the animation!
  if (key === 's') {
    saveGif('mySketch', 5);
  }
}