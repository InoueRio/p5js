let NORTH = 0;
let EAST  = 1;
let SOUTH = 2;
let WEST  = 3;
let direction = NORTH;

let ang;
let ang_step = 10;
let min_len = 10;

let pos_x;
let pos_y;
let pos_x_cross;
let pos_y_cross;

let margin = 5;
let reached_border;
let draw_line_bool;

function setup() {
//  createCanvas(windowWidth, windowHeight);
  createCanvas(400, 400);
  colorMode(HSB, 360, 100, 100, 100);
  background(360);

  ang = get_random_ang(direction);
  pos_x = random(width);
  pos_y = margin;
  pos_x_cross = pos_x;
  pos_y_cross = pos_y;
}

function draw() {
  let spd;
  spd = int(map(mouseX, 0, width, 1, 10));
  for (let i=0; i<spd; i++){

    // draw current position
    strokeWeight(2);
    stroke_alpha = (draw_line_bool==true)? 0:100;
    draw_line_bool = false;
    console.log("i=" + i);
    stroke(180, 0, 0, stroke_alpha);
    point(pos_x, pos_y);

    // make reach point
    pos_x += cos(radians(ang))*ang_step;
    pos_y += sin(radians(ang))*ang_step;

    // check if posion is near border
    reached_border = false;
    if (pos_y < margin){
      direction = SOUTH;
      reached_border = true;
    } else if (pos_y > height - margin){
      direction = NORTH;
      reached_border = true;
    } else if (pos_x < margin){
      direction = EAST;
      reached_border = true;
    } else if (pos_x > width - margin){
      direction = WEST;
      reached_border = true;
    }

    // draw
    loadPixels();
    let current_pixel = get(floor(pos_x), floor(pos_y));
    if (
      reached_border 
      || (current_pixel[0] != 255 && current_pixel[1] != 255 && current_pixel[2] != 255)
    ) {
      ang = get_random_ang(direction);
      let d = dist(pos_x, pos_y, pos_x_cross, pos_y_cross);
      if (d > min_len){
        draw_line(pos_x, pos_y, pos_x_cross, pos_y_cross);
        draw_line_bool = true;
      }

      pos_x_cross = pos_x;
      pos_y_cross = pos_y;
    }
  }

}

function get_random_ang(current_direction){
  let ang_tmp = random(-ang_step, ang_step)*90/ang_step;
  switch (current_direction){
    case NORTH:
      return ang_tmp - 90;
    case EAST:
      return ang_tmp;
    case SOUTH:
      return ang_tmp + 90;
    case WEST:
      return ang_tmp + 180;
  }
}

function draw_line(pos_x, pos_y, pos_x_cross, pos_y_cross){
  
  // console.log(switch_val);

  distance = dist(pos_x, pos_y, pos_x_cross, pos_y_cross); // distance
  distance_max = dist(0, 0, width, height);
  storke_weight_val = round(map(distance, 0, distance_max, 1, 10)); //vary with distance
  hue_val = 195;
  saturation_val = 100;
  brightness_val = map(distance, 0, distance_max, 10, 100); //vary with distance
  
  // console.log(hue_val + "," + saturation_val + "," + brightness_val);
  strokeWeight(storke_weight_val);
  stroke(hue_val, saturation_val, brightness_val);
  line(pos_x, pos_y, pos_x_cross, pos_y_cross);
}


function keyPressed() {
  if (key === 's') {
    saveCanvas('myCanvas', 'png');
  }
  if (key === 'S') {
    saveGif('mySketch', 3);
  }
}