let tile_count = 20;
let random_seed = 0;
let rect_size = 20;
hue_value = 60;

function setup() {
//  createCanvas(windowWidth, windowHeight);
  createCanvas(400, 400);
  colorMode(HSB, 360, 100, 100, 100);
  noStroke();
}

function draw() {
  clear();
  randomSeed(random_seed);
  fill(hue_value, 80, 80, 60);

  for(let gridX=0; gridX<tile_count; gridX++){
    for(let gridY=0; gridY<tile_count; gridY++){
      pos_x = width / tile_count * gridX;
      pos_y = height / tile_count * gridY;
      
      random_val = 1
      div = 20
      let shift_x = [];
      let shift_y = [];
      for(let i = 0; i<4; i++){ // create square vertex, point is 4
        shift_x[i] = mouseX/div * random(-random_val, random_val);
        shift_y[i] = mouseY/div * random(-random_val, random_val);
      }
      // console.log("pos_x:"+pos_x + ",pos_y:" + pos_y);
      push();
      translate(pos_x, pos_y);
      beginShape();
        // create square
        vertex(shift_x[0], shift_y[0]); // vertex is 頂点 in japanese  
        vertex(rect_size + shift_x[1], shift_y[1]);
        vertex(rect_size + shift_x[2], rect_size + shift_y[2]);
        vertex(shift_x[3], rect_size + shift_y[3]);
      endShape();
      pop();
    }
    
  }
  // console.log("test");
}

function mousePressed() {
  random_seed = random(100);
  hue_value = random(0, 100);
}

function keyPressed() {
  if (key === 's') {
    saveGif('01.gif', 5); // 1 sec
  }
  // saveGif('02.gif', 1); 
}