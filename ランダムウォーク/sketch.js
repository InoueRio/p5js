
let x;
let y;

function setup() {
  createCanvas(400, 400);
  colorMode(HSB, 360, 100, 100, 100);
  blendMode(ADD);
  x = width / 2;
  y = height / 2;

  background(0);
}

function draw() {
  for (let i = 0; i < 100; i = i + 1) {
    stroke((frameCount + i * 0.01) % 360, 80, 100, 5);
    point(x, y);
    x = x + random(-1, 1);
    y = y + random(-1, 1);

    if (x > width) {
      x = x - width;
    }
    if (x < 0) {
      x = x + width;
    }
    if (y > height) {
      y = y - height;
    }
    if (y < 0) {
      y = y + height;
    }
  }
}

function keyPressed() {
  // this will download the first 5 seconds of the animation!
  if (key === 'g') {
    saveGif('mySketch', 5);
  }
  else if(key == 'c'){
    saveCanvas('myCanvas', 'jpg');
  }
}