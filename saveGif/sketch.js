const frmRate = 30;

function setup() {
  createCanvas(480, 480);
  smooth();
  frameRate(frmRate);
  fill(96);
  noStroke();
}

function draw() {
  const t = map(frameCount % frmRate, 0, frmRate, 0.0, 1.0);
  const x = width * 0.4 * cos(TWO_PI * t);
  const y = height * 0.4 * sin(TWO_PI * t);

  background(240);
  translate(width * 0.5, height * 0.5);
  circle(x, y, 50);
}

function keyPressed() {
  if (keyCode === 's') {
    saveGif('01.gif', 1); // 1 sec
  }
  saveGif('02.gif', 1); 
}
