function setup() {
  createCanvas(400, 400);
  noStroke();
  colorMode(HSB, width, height, 100);
}

function draw() {
  step_x = mouseX+1;
  step_y = mouseY+1;
  for (let grid_y=0; grid_y<height; grid_y=grid_y+step_y){
    for (let grid_x=0; grid_x<width; grid_x=grid_x+step_x){
      fill(grid_x, grid_y, 100);
      rect(grid_x, grid_y, step_x, step_y);
    }
  }
}

function keyPressed() {
  // this will download the first 5 seconds of the animation!
  if (key === 's') {
    saveCanvas("myCanvas", "jpg");
  }
  if (key === 'g') {
    saveGif('01.gif', 10); // 1 sec
  }
}