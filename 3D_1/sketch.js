
//箱の配列

let x = [];
let y = [];
let z = [];
let c = [];

let num = 400;

function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL);
  colorMode(HSB, 360, 100, 100, 100);
  angleMode(DEGREES);

  for (let i = 0; i < num; i = i + 1) {
    x[i] = random(-width / 2, width / 2);
    y[i] = random(-height / 2, width / 2);
    z[i] = random(-500, 500);
    let n = floor(random(5));
    let h = map(n, 0, 4, 250, 360); //ランダムに作った値をベースに色相を生成
    c[i] = color(h, 200, 200);
  }
}


function draw() {
  background(0, 0, 10);

  orbitControl();

  ambientLight(0, 0, 1000);
  directionalLight(0, 0, 1000, 0, 0, -1);
  for (let i = 0; i < num; i = i + 1) {
    push();
    translate(x[i], y[i], z[i]);
    ambientMaterial(c[i]);
    let m = x[i] + y[i] + z[i] + frameCount;
    rotateX(m/3);
    rotateY(m/4);
    rotateZ(m/5);

    sphere(10, 100, 100);
    pop();
    //Z軸方向に移動
    z[i] = z[i] + 5;
    //カメラから映らなくなったら画面奥へ移動
    if (z[i] > 500) {
      z[i] = -500;
    }
  }
}

// you can put it in the mousePressed function,
// or keyPressed for example
function keyPressed() {
  // this will download the first 5 seconds of the animation!
  if (key === 's') {
    saveGif('3D\2', 2);
  }
}