let NORTH = 0;
let EAST  = 1;
let SOUTH = 2;
let WEST  = 3;
let direction = NORTH

let ang;
let ang_step = 10;
let min_len = 10;

let pos_x;
let pos_y;
let pos_x_cross;
let pos_y_cross;

let margin = 5;
let reached_border;

function setup() {
//  createCanvas(windowWidth, windowHeight);
  createCanvas(windowWidth, windowHeight);
  colorMode(HSB, 360, 100, 100, 100);
  background(360);

  ang = get_random_ang(direction);
  pos_x = random(width);
  pos_y = margin;
  pos_x_cross = pos_x;
  pos_y_cross = pos_y;
}

function draw() {
  let speed = int(map(mouseX, 0, width, 1, 5));
  for (let i=0; i<speed; i++){

    // draw current position
    strokeWeight(3);
    stroke(180, 0, 0);
    point(pos_x, pos_y);

    // make reach point
    pos_x += cos(radians(ang))*ang_step;
    pos_y += sin(radians(ang))*ang_step;

    // check if posion is near border
    reached_border = false;
    if (pos_y < margin){
      direction = SOUTH;
      reached_border = true;
    } else if (pos_y > height - margin){
      direction = NORTH;
      reached_border = true;
    } else if (pos_x < margin){
      direction = EAST;
      reached_border = true;
    } else if (pos_x > width - margin){
      direction = WEST;
      reached_border = true;
    }

    // draw
    loadPixels();
    let current_pixel = get(floor(pos_x), floor(pos_y));
    if (
      reached_border ||
      (current_pixel[0] != 255 && current_pixel[1] != 255 && current_pixel[2] != 255)
    ) {
      ang = get_random_ang(direction);
      let d = dist(pos_x, pos_y, pos_x_cross, pos_y_cross);
      if (d > min_len){
        strokeWeight(3);
        stroke(0, 0, 0);
        line(pos_x, pos_y, pos_x_cross, pos_y_cross);
      }

      pos_x_cross = pos_x;
      pos_y_cross = pos_y;
    }
  }

}

function get_random_ang(current_direction){
  let ang_tmp = random(-ang_step, ang_step)*90/ang_step;
  switch (current_direction){
    case NORTH:
      return ang_tmp - 90;
    case EAST:
      return ang_tmp;
    case SOUTH:
      return ang_tmp + 90;
    case WEST:
      return ang_tmp + 180;
  }
}


function keyPressed() {
  if (key === 's') {
    saveCanvas('myCanvas', 'png');
  }
  if (key === 'S') {
    saveGif('mySketch', 3);
  }
}